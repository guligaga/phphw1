<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arr3</title>
</head>
<body>
<a href="./index.php">Return to Arrays menu</a><br><br>
<h3>Arrays Task3</h3>
<h4>Найти индекс минимального элемента массива .</h4>
<p>You can copy this array: [14, 88, 2, 23, 70, 1, 12, 20]</p>
<form method='post' id="post-form">
    <!--    <input type="text" name="inputA" size="40" maxlength="4" placeholder='Whole arr' style="font-size: 16px">-->
    <span style="font-size: 20px; font-weight: bold">[<span/>
    <input type="text" name="0" size="40" maxlength="4" placeholder='0' style="font-size: 16px; width: 45px">
    <input type="text" name="1" size="40" maxlength="4" placeholder='1' style="font-size: 16px; width: 45px">
    <input type="text" name="2" size="40" maxlength="4" placeholder='2' style="font-size: 16px; width: 45px">
    <input type="text" name="3" size="40" maxlength="4" placeholder='3' style="font-size: 16px; width: 45px">
    <input type="text" name="4" size="40" maxlength="4" placeholder='4' style="font-size: 16px; width: 45px">
    <input type="text" name="5" size="40" maxlength="4" placeholder='5' style="font-size: 16px; width: 45px">
    <input type="text" name="6" size="40" maxlength="4" placeholder='6' style="font-size: 16px; width: 45px">
    <input type="text" name="7" size="40" maxlength="4" placeholder='7' style="font-size: 16px; width: 45px">
    <span style="font-size: 20px; font-weight: bold">]<span/><br><br>
    <input type='submit' size="40" value="Get result">
</form>
<br><br>
<?php

function arrays3($num){
    $min =  min(array_filter($num, function($value) {
        return ($value !== null && $value !== false && $value !== '');
    }));
    return array_search($min, $num);
}
if(!empty($_POST)) {
    echo 'Min value\'s index is: ';
    echo arrays3($_POST);
}
?>
</body>
</html>
