<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arr8</title>
</head>
<body>
<a href="./index.php">Return to Arrays menu</a><br><br>
<h3>Arrays Task8</h3>
<h4>Поменять местами первую и вторую половину массива, например, для массива 1 2 3  4, результат 3 4 1 2.</h4>
<p>You can copy this array: [14, 88, 2, 23, 70, 1, 12, 20]</p>
<form method='post' id="post-form">
    <!--    <input type="text" name="inputA" size="40" maxlength="4" placeholder='Whole arr' style="font-size: 16px">-->
    <span style="font-size: 20px; font-weight: bold">[<span/>
    <input type="text" name="0" size="40" maxlength="4" placeholder='0' style="font-size: 16px; width: 45px">
    <input type="text" name="1" size="40" maxlength="4" placeholder='1' style="font-size: 16px; width: 45px">
    <input type="text" name="2" size="40" maxlength="4" placeholder='2' style="font-size: 16px; width: 45px">
    <input type="text" name="3" size="40" maxlength="4" placeholder='3' style="font-size: 16px; width: 45px">
    <input type="text" name="4" size="40" maxlength="4" placeholder='4' style="font-size: 16px; width: 45px">
    <input type="text" name="5" size="40" maxlength="4" placeholder='5' style="font-size: 16px; width: 45px">
    <input type="text" name="6" size="40" maxlength="4" placeholder='6' style="font-size: 16px; width: 45px">
    <input type="text" name="7" size="40" maxlength="4" placeholder='7' style="font-size: 16px; width: 45px">
    <span style="font-size: 20px; font-weight: bold">]<span/><br><br>
    <input type='submit' size="40" value="Get result">
</form>
<br><br>
<?php

function arrays8($num){
    $amount = count(array_filter($num, function($value) {
        return ($value !== null && $value !== false && $value !== '');
    }));

    if ($amount % 2 == 1) {
        list($a, $b) = array_chunk($num, count($num)/2);
        $c[0] = array_pop($a);
        return array_merge($b, $c, $a);
    }
    list($a, $b) = array_chunk($num, count($num)/2);
    return array_merge($b, $a);
}
if(!empty($_POST)) {
    echo 'Result is: ';
    print_r(arrays8($_POST));
}
?>
</body>
</html>
