<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arr9</title>
</head>
<body>
<a href="./index.php">Return to Arrays menu</a><br><br>
<h3>Arrays Task9</h3>
<h4>Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert)).</h4>
<p>You can copy this array: [14, 88, 2, 23, 70, 1, 12, 20]</p>
<form method='post' id="post-form">
    <!--    <input type="text" name="inputA" size="40" maxlength="4" placeholder='Whole arr' style="font-size: 16px">-->
    <span style="font-size: 20px; font-weight: bold">[<span/>
    <input type="text" name="0" size="40" maxlength="4" placeholder='0' style="font-size: 16px; width: 45px">
    <input type="text" name="1" size="40" maxlength="4" placeholder='1' style="font-size: 16px; width: 45px">
    <input type="text" name="2" size="40" maxlength="4" placeholder='2' style="font-size: 16px; width: 45px">
    <input type="text" name="3" size="40" maxlength="4" placeholder='3' style="font-size: 16px; width: 45px">
    <input type="text" name="4" size="40" maxlength="4" placeholder='4' style="font-size: 16px; width: 45px">
    <input type="text" name="5" size="40" maxlength="4" placeholder='5' style="font-size: 16px; width: 45px">
    <input type="text" name="6" size="40" maxlength="4" placeholder='6' style="font-size: 16px; width: 45px">
    <input type="text" name="7" size="40" maxlength="4" placeholder='7' style="font-size: 16px; width: 45px">
    <span style="font-size: 20px; font-weight: bold">]<span/><br><br>
    <input type='submit' size="40" value="Get result">
</form>
<br><br>
<?php

function bubbleSort($num) {
    $start = microtime(true);
    $amount = count($num);
    for ($i = 0; $i < count($num); $i++) {
        $amount--;
        for ($j = 0; $j < $amount; $j++) {
            if ($num[$j] > $num[$j + 1]) {
                $temp = $num[$j];
                $num[$j] = $num[$j + 1];
                $num[$j + 1] = $temp;
            }
        }
    }
    echo 'B Time spent: ' . (microtime(true) - $start);
    echo '<br><br>';
    echo 'Bubble sort result is: ';
    return $num;
}

function insertionSort($num){
    $start = microtime(true);
    for ($i = 1; $i < count($num); $i++) {

        $valueToSort = $num[$i];
        $j = $i;

        while ($j > 0 && $valueToSort < $num[$j - 1]) {

            $num[$j] = $num[$j - 1];
            $j--;
        }

        $num[$j] = $valueToSort;
    }
    echo 'I Time spent: ' . (microtime(true) - $start);
    echo '<br><br>';
    echo 'Insertion sort result is: ';
    return $num;
}
if(!empty($_POST)) {
    print_r(bubbleSort($_POST));
    echo '<br><br>';

//    echo 'Selection sort result is: ';
//    print_r(insertionSort($_POST));
//    echo '<br><br>';

    print_r(insertionSort($_POST));
}
?>
</body>
</html>
