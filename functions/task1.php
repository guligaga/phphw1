<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fun1</title>
</head>
<body>
<a href="./index.php">Return to Functions menu</a><br><br>
<h3>Functions Task1</h3>
<h4>Получить строковое название дня недели по номеру дня.</h4>
<form method='post' id="post-form">
    <input type="text" name="inputNum" size="40" maxlength="4" placeholder='Day number' style="font-size: 16px; width: 100px">
    <input type='submit' size="40" value="Get result">
</form>
<h3>
<?php

function functions1($num) {
    intval($num);
    switch ($num) {
    case 1:
        return 'Monday';
    case 2:
        return 'Tuesday';
    case 3:
        return 'Wednesday';
    case 4:
        return 'Thursday';
    case 5:
        return 'Friday';
    case 6:
        return 'Saturday';
    case 7:
        return 'Sunday';
    default:
        return 'Undefinedday';
    }
}
if(!empty($_POST)) {
    echo 'It is: ';
    echo functions1($_POST['inputNum']);
}
?>
</h3>
</body>
</html>
