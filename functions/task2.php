<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fun2</title>
</head>
<body>
<a href="./index.php">Return to Functions menu</a><br><br>
<h3>Functions Task2</h3>
<h4>Вводим число (0-999), получаем строку с прописью числа.</h4>
<form method='post' id="post-form">
    <input type="text" name="inputNum" size="40" maxlength="4" placeholder='Day number' style="font-size: 16px; width: 100px">
    <input type='submit' size="40" value="Get result">
</form>
<h3>
    <?php
    function getOnes($o) {
        $ones = ['zero', ' one', ' two', ' three', ' four', ' five', ' six', ' seven', ' eight', ' nine'];
        return $ones[$o];
    }
    function getTens ($t, $o) {
        $teens = [' ten', ' eleven', ' twelve', ' thirteen', ' fourteen', ' fifteen', ' sixteen', ' seventeen', ' eighteen', ' nineteen'];
        $tens = ['', ' ten', ' twenty', ' thirty', ' fourty', ' fifty', ' sixty', ' seventy', ' eighty', ' ninety'];
        if($t == 1) {
            return $teens[$o];
        }
        return $tens[$t] . getOnes($o);
    }
    function getHundreds($h, $t, $o) {
        $hundreds = ['', 'one hundred', 'two hundred', 'three hundred', 'four hundred',
        'five hundred', 'six hundred', 'seven hundred', 'eight hundred', 'nine hundred'];
        return $hundreds[$h] . ' and' . getTens($t, $o);
    }
    function getWord ($num) {
        $number = array_filter(str_split($num), function($value) {
            return ($value !== null && $value !== false && $value !== '');
        });
        $word = '';
        switch (count($number)) {
        case 1:
            $word = getOnes($number[0]);
            break;
        case 2:
            $word = getTens($number[0], $number[1]);
            break;
        case 3:
            $word = getHundreds($number[0], $number[1], $number[2]);
            break;
        }
        return $word;
    }
    function functions2($num) {
        $num = intval($num);
        switch ($num) {
            case 100:
                return 'one hundred';
            default:
                return getWord($num);
        }

    }
    if(!empty($_POST)) {
        echo 'It is: ';
        echo functions2($_POST['inputNum']);
    }
    ?>
</h3>
</body>
</html>

