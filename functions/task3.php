<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fun2</title>
</head>
<body>
<a href="./index.php">Return to Functions menu</a><br><br>
<h3>Functions Task2</h3>
<h4>Вводим число (0-999), получаем строку с прописью числа.</h4>
<form method='post' id="post-form">
    <input type="text" name="inputNum" size="40" maxlength="4" placeholder='Day number' style="font-size: 16px; width: 100px">
    <input type='submit' size="40" value="Get result">
</form>
<h3>
    <?php
    function functions2($num) {
        $num = intval($num);
        switch ($num) {
        case 100:
            return 'one hundred';
        default:
            return getWord($num);
        }

    }
    if(!empty($_POST)) {
        echo 'It is: ';
        echo functions2($_POST['inputNum']);
    }
    ?>
</h3>
</body>
</html>

