<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fun4</title>
</head>
<body>
<a href="./index.php">Return to Functions menu</a><br><br>
<h3>Functions Task4</h3>
<h4>Найти расстояние между двумя точками в двумерном декартовом пространстве.</h4>
<form method='post' id="post-form">
    <span style="font-size: 20px; font-weight: bold">Enter A(<span/>
    <input type="text" name="Ax" size="40" maxlength="4" placeholder='x' style="font-size: 16px; width: 45px">
    <input type="text" name="Ay" size="40" maxlength="4" placeholder='y' style="font-size: 16px; width: 45px">
    <span style="font-size: 20px; font-weight: bold">)<span/><br><br>

    <span style="font-size: 20px; font-weight: bold">Enter B(<span/>
    <input type="text" name="Bx" size="40" maxlength="4" placeholder='x' style="font-size: 16px; width: 45px">
    <input type="text" name="By" size="40" maxlength="4" placeholder='y' style="font-size: 16px; width: 45px">
    <span style="font-size: 20px; font-weight: bold">)<span/><br><br>
    <input type='submit' size="40" value="Get result">
</form>
<br><br>
<?php

function functions4($arr){
    return sqrt(pow(($arr['Ax'] - $arr['Bx']), 2) + pow(($arr['Ay'] - $arr['By']), 2));
}
if(!empty($_POST)) {
    echo "Distance between A({$_POST['Ax']}, {$_POST['Ay']}) and B({$_POST['Bx']}, {$_POST['By']}) is: ";
    echo functions4($_POST);
}
?>
</body>
</html>
