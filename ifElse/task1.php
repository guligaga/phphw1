<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iETask1</title>
</head>
<body>
<a href="./index.php">Return to IfElse menu</a><br><br>
<h3>iElse Task1</h3>
<h4>Если а – четное посчитать а*б, иначе а+б</h4>
<form method='post' id="post-form">
 <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type="text" name="inputB" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function ifElse1($a, $b){
    if(is_numeric($a) && is_numeric($b)) {

        if($a % 2 === 0) {
            return $a * $b;
        }
        else if ($a % 2 === 1) {
            return $a + $b;
        }
    }
    return 'You did\'t enter numbers';
}
 
if(isset($_POST['inputA'])) {
    echo 'Result is: ';
    echo ifElse1($_POST["inputA"],$_POST["inputB"]);
}
?>
</body>
</html>
