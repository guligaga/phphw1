<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iETask2</title>
</head>
<body>
<a href="./index.php">Return to IfElse menu</a><br><br>
<h3>iElse Task2</h3>
<h4>Определить какой четверти принадлежит точка с координатами (х,у)</h4>
<form method='post' id="post-form">
 <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type="text" name="inputB" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function ifElse2($x, $y){
    if(is_numeric($x) && is_numeric($y)) {
        if($x >= 0) {
            if($y >= 0) {
                return 'I quarter';
            }
            else {
                return 'IV quarter';
            }
        }
        else {
            if($y >= 0) {
                return 'II quarter';
            }
            else {
                return 'III quarter';
            }
        }
    }
    return 'You didn\'t enter numbers';
}
if(isset($_POST['inputA'])) {
    echo 'Result is: ';
    echo ifElse2($_POST["inputA"],$_POST["inputB"]);
}

?>
</body>
</html>
