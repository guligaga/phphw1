<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iETask3</title>
</head>
<body>
<a href="./index.php">Return to IfElse menu</a><br><br>
<h3>iElse Task3</h3>
<h4>Найти суммы только положительных из трех чисел</h4>
<form method='post' id="post-form">
 <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type="text" name="inputB" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type="text" name="inputC" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function ifElse3($a, $b, $c){
    if(is_numeric($a) && is_numeric($b) && is_numeric($c)) {
        
        $a = ($a >= 0)? $a : 0;
        $b = ($b >= 0)? $b : 0;
        $c = ($c >= 0)? $c : 0;
        return $a + $b + $c;
    }
    return 'You did\'t enter numbers';
}
 
if(isset($_POST['inputA'])) {
    echo 'Result is: ';
    echo ifElse3($_POST["inputA"], $_POST["inputB"], $_POST["inputC"]);
}
?>
</body>
</html>
