<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iETask4</title>
</head>
<body>
<a href="./index.php">Return to IfElse menu</a><br><br>
<h3>iElse Task4</h3>
<h4>Посчитать выражение макс(а*б*с, а+б+с)+3</h4>
<form method='post' id="post-form">
 <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type="text" name="inputB" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type="text" name="inputC" size="40" maxlength="35" placeholder='Enter value' style="font-size: 16px"><br><br>
 <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function ifElse3($a, $b, $c){
    if(is_numeric($a) && is_numeric($b) && is_numeric($c)) {
        $maxVal = max(($a * $b * $c), ($a + $b + $c));
        return $maxVal + 3;
    }
    return 'You did\'t enter numbers';
}
 
if(isset($_POST['inputA'])) {
    echo 'Result is: ';
    echo ifElse3($_POST["inputA"], $_POST["inputB"], $_POST["inputC"]);
}
?>
</body>
</html>
