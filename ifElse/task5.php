<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iETask5</title>
</head>
<body>
<a href="./index.php">Return to IfElse menu</a><br><br>
<h3>iElse Task5</h3>
<h4>Написать программу определения оценки студента по его рейтингу</h4>
<form method='post' id="post-form">
 <input type="text" name="inputA" size="40" maxlength="35" placeholder="Enter student's rating" style="font-size: 16px"><br><br>
 <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function ifElse3($rat){
    if ($rat >= 0 && $rat <= 19)
        return 'F';
    else if ($rat >= 20 && $rat <= 39)
        return 'E';
    else if ($rat >= 40 && $rat <= 59)
        return 'D';
    else if ($rat >= 60 && $rat <= 74)
        return 'C';
    else if ($rat >= 75 && $rat <= 89)
        return 'B';
    else if ($rat >= 90 && $rat <= 100)
        return 'A';
    else
        return 'You did\'t enter numbers';
} 
if(isset($_POST['inputA'])) {
    echo 'Result is: ';
    echo ifElse3($_POST["inputA"]);
}
?>
</body>
</html>
