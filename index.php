<?php
echo '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP HW1</title>
</head>
<body>
<h1>My PHP HW1</h1>
    <ul>
        <li>
            <a href="/ifElse">IfElse</a>
        </li>
        <li>
            <a href="/loops">Loops</a>
        </li>
        <li>
            <a href="/arrays">Arrays</a>
        </li>
        <li>
            <a href="/functions">Functions</a>
        </li>
    </ul>
</body>
</html> 
';
?>