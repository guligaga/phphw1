<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ltask1</title>
</head>
<body>
<a href="./index.php">Return to Loops menu</a><br><br>
<h3>Loops Task1</h3>
<h4>Найти сумму четных чисел и их количество в диапазоне от 1 до 99</h4>
<form method='post' id="post-form">
 <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter final value of range' style="font-size: 16px"><br><br>
 <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function loops1($final){
    $count = 0;
    $sum = 0;
    if(is_numeric($final)) {
        for ($i = 1; $i < $final; $i++) {
            if ($i % 2 === 0) {
                $count++;
                $sum += $i;
            }
        }
        return "Sum is: $sum, Amount is: $count";
    }
    return 'You did\'t enter numbers';
}
 
if(isset($_POST['inputA'])) {
    echo loops1($_POST["inputA"]);
}
?>
</body>
</html>
