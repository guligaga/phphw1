<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ltask2</title>
</head>
<body>
<a href="./index.php">Return to Loops menu</a><br><br>
<h3>Loops Task2</h3>
<h4>Проверить простое ли число? (число называется простым, если оно делится только само на себя и на 1)</h4>
<form method='post' id="post-form">
 <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter cheked value' style="font-size: 16px"><br><br>
 <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function loops2($num){
    $count = 0;
    if(is_numeric($num)) {
        for ($i = 1; $i < $num; $i++) {
            if($num % $i == 0) {
                $count++;
            }
        }
        if($count <= 2) {
            return "$num is simple";
        }
        return "$num is NOT simple";
    }
    return 'You did\'t enter numbers';
}
 
if(isset($_POST['inputA'])) {
    echo loops2($_POST["inputA"]);
}
?>
</body>
</html>
