<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ltask3</title>
</head>
<body>
<a href="./index.php">Return to Loops menu</a><br><br>
<h3>Loops Task3</h3>
<h4>Найти корень натурального числа с точностью до целого (рассмотреть вариант последовательного подбора и метод бинарного поиска)</h4>
<form method='post' id="post-form">
    <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter value'"><br><br>
    <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function loops3Serial($num){
    if(is_numeric($num)) {
        for($i = 1; $i <= $num/2; $i++) {
            if ($i  == round($num / $i)) {
                return $i;
            }
        }
    }
    return 'You didn\'t enter numbers';
}
//function loops3Binary($num){
//    if(is_numeric($num)) {
//        $mid = $num;
//        while ($mid < $num) {
//            $mid /= 2;
//            $rightMid = intval($mid);
//            if ($rightMid * $rightMid < $num) {
//                $mid += $rightMid * 2;
//            }
//            elseif ($rightMid = $num / $rightMid) {
//                return $rightMid;
//            }
//        }
//    }
//    return 'You didn\'t enter numbers';
//}
 
if(isset($_POST['inputA'])) {
    echo 'Serial search result: ';
    echo loops3Serial($_POST["inputA"]);
//    echo '<br>Binary search result: ';
//    echo loops3Binary($_POST["inputA"]);
}


?>
</body>
</html>
