<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ltask4</title>
</head>
<body>
<a href="./index.php">Return to Loops menu</a><br><br>
<h3>Loops Task4</h3>
<h4>Вычислить факториал числа n. n! = 1*2*…*n-1*n;!</h4>
<form method='post' id="post-form">
    <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter cheked value' style="font-size: 16px"><br><br>
    <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function loops4($num){
    if(is_numeric($num)) {
        $fac = 1;
        for ($i = 1; $i <= $num; $i++) {
            $fac *= $i;
        }
        return $fac;
    }
    return 'You did\'t enter numbers';
}

if(isset($_POST['inputA'])) {
    echo loops4($_POST["inputA"]);
}
?>
</body>
</html>
