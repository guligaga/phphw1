<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ltask5</title>
</head>
<body>
<a href="./index.php">Return to Loops menu</a><br><br>
<h3>Loops Task5</h3>
<h4>Посчитать сумму цифр заданного числа</h4>
<form method='post' id="post-form">
    <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter cheked value' style="font-size: 16px"><br><br>
    <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function loops5($num){
    if(is_numeric($num)) {
        $numeral = str_split($num);
        $sum = 0;
        for ($i = 0; $i < strlen($num); $i++) {
            $sum += $numeral[$i];
        }
        return $sum;
    }
    return 'You did\'t enter numbers';
}

if(isset($_POST['inputA'])) {
    echo loops5($_POST["inputA"]);
}
?>
</body>
</html>
