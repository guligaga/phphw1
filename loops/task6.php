<?php  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ltask6</title>
</head>
<body>
<a href="./index.php">Return to Loops menu</a><br><br>
<h3>Loops Task6</h3>
<h4>Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например, задано число 123, вывести 321.</h4>
<form method='post' id="post-form">
    <input type="text" name="inputA" size="40" maxlength="35" placeholder='Enter cheked value' style="font-size: 16px"><br><br>
    <input type='submit' size="40" value="Get result">
</form>
<br>
<?php

function loops6($num){
    if(is_numeric($num)) {
        $numeral = array_reverse(str_split($num));
        $rev = '';
        for ($i = 0; $i < count($numeral); $i++) {
            $rev .= $numeral[$i];
        }
        return $rev;
    }
    return 'You did\'t enter numbers';
}

if(isset($_POST['inputA'])) {
    echo loops6($_POST["inputA"]);
}
?>
</body>
</html>
